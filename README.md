# TaxiDestinationPrediction

This repo contains the code to reproduce all the experiments described in the paper:
Rossi, Alberto, et al. "Modelling Taxi Drivers' Behaviour for the Next Destination Prediction." IEEE Transactions on Intelligent Transportation Systems (2019).


## Requirements

This project is entirely build with Python (version>=3.3).
In order to run all the experiment you need to install the following Python packages.

* Pandas
* Numpy
* Scikit-learn
* Keras
* Tensorflow
* Gensim

## Project Structure

In this repo are present three subfolder, one for each city tested (Porto, Manhattan and San Francisco)

Each of that are organized as follows:

*  **Main folder**: contains general tool to manipulate data in order to make it suitable to the experiment.
*  **Data**: contains data necessary to run the experiments.
*  **Models**: host all the model we have tested.

For the city of San Francisco, since we cannot release the data, we give the link to website hosting those data, and all the instruments to manipulate the data, in order to build the experiment reproducible.