# Data Preprocessing

In order to run the experiment you nedd to download the Taxi's trips data of [San Francisco](https://crawdad.org/epfl/mobility/20090224/)

After that you have to run the following notebook following the proposed order:

* **Analize&Parse** Preliminaries analisis and outlayers removing
* **ClusteringKMeans** Trip clustering and assign cluster to each trip.
* **IndividualSequence** Produce the individual sequence
* **ShuffleSequence** Shuffle sequence in order to mix driver's sequence
* **W2VModel** Build the W2V representation for each cell and assign this representation to all the dataset
* **ReconstructSequencePickupDropoff** Split each trip (Pickup-Dropoff) to a single entry (either Pickup or Dropoff)

To reproduce the competition winning model experiments, as it the paper of [Brébisson et. al.](https://arxiv.org/abs/1508.00021) you need to exctract 10 points by each trip trajectory running the following notebook.

* **Get10Points**
