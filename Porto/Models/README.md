This folder contain the model tested in the paper.

In particular you can find:

* LSTM 
* LSTM with POIs
* LSTM with POIs and W2V cluster embedding

Moreover we provide two different version of the competition winning model:

* MLP_MILA: model identical as the competion winnign model.
* MLP_SEQ: MLP taking as input the driver's sequence linearized.

At the end the baseline model is a nearest neighbors model, that return as a destination, the coordinates of the closer cluster, different from the starting one.

* Baseline
